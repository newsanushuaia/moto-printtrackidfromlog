﻿namespace PrintTrackIdFromLog
{
    public interface IDirSweeper
    {
        void SweepFiles();

        void StopSweeper();
    }
}
