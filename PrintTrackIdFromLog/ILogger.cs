﻿namespace PrintTrackIdFromLog
{
    using System;

    public interface ILogger
    {
        void Debug(string text);

        void Warn(string text);

        void Error(string text);

        void Error(Exception ex);
    }
}
