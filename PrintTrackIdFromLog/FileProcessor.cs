﻿namespace PrintTrackIdFromLog
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Threading;

    public class FileProcessor : IFileProcessor
    {
        #region Properties
        private readonly ILogger logger;
        private readonly string filePath;
        private readonly string fileName;
        private readonly int sleepTime;
        private readonly int retryCount;
        //private readonly string ZplPrint = @"^XA~TA000~JSN^MNW^MTT^PON^PMN^JMA^JUS^LRN^CI0^XZ^XA^MMT^PW203^LL0117^FT50,30^A0N,20,20^FH\^FD$trackid^FS^FT30,80^BY1,3,40^BCN,,N,N ^FD$trackid^FS ^PQ1,0,1,Y^XZ";
        #endregion

        #region Constructor
        public FileProcessor(ILogger logger, string filePath, string fileName, int sleepTime, int retryCount)
        {
            this.logger = logger ?? throw new ArgumentNullException("ILogger");
            this.filePath = filePath ?? throw new ArgumentNullException("filePath");
            this.fileName = fileName ?? throw new ArgumentNullException("fileName");
            this.sleepTime = sleepTime;
            this.retryCount = retryCount;
        }
        #endregion

        #region IFileProcessor Methods
        public void ProcessFile()
        {
            try
            {
                var counter = 1;
                Thread.Sleep(sleepTime);

                var trackid = String.Empty;

                trackid = this.fileName.Substring(this.fileName.IndexOf("ZY"), 10);

                var printer = ConfigurationManager.AppSettings.Get("printer");

                var ZplPrint = ConfigurationManager.AppSettings.Get("ZPL");

                var etiqueta = ZplPrint.Replace("$trackid", trackid);

                if (!string.IsNullOrEmpty(printer))
                {
                    logger.Debug(("Imprimio"));
                    new Com.SharpZebra.Printing.ZebraPrinter(printer).Print(etiqueta);

                }


            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void DeleteFile()
        {
            try
            {
                var counter = 1;
                Thread.Sleep(sleepTime);

                while (IsFileLocked(new FileInfo(filePath)) && (counter < retryCount))
                {
                    ++counter;
                    Thread.Sleep(sleepTime);
                }

                if (counter == retryCount)
                {
                    logger.Warn(string.Format("File {0} locked. Retried {1} times with no success.", fileName, counter.ToString()));
                    return;
                }

                File.Delete(filePath);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
        #endregion

        #region Private Methods
        

        private bool IsFileLocked(FileInfo file)
        {
            var stream = default(FileStream);

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                // the file is unavailable because it is still being written
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            return false;
        }
        #endregion
    }
}
