﻿namespace PrintTrackIdFromLog
{
    public interface IFileProcessor
    {
        void ProcessFile();

        void DeleteFile();
    }
}
