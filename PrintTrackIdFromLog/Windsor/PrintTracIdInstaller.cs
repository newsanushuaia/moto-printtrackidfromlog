﻿namespace PrintTrackIdFromLog.Windsor
{
    using Castle.MicroKernel.Registration;
    using Castle.MicroKernel.SubSystems.Configuration;
    using Castle.Windsor;

    public class PrintTracIdInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(
                Classes.FromThisAssembly()
                .InNamespace("PrintTrackIdFromLog")
                .WithService.DefaultInterfaces()
                .LifestyleTransient());
        }
    }
}
