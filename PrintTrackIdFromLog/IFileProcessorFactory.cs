﻿namespace PrintTrackIdFromLog
{
    public interface IFileProcessorFactory
    {
        IFileProcessor Create(ILogger logger, string filePath, string fileName, int sleepTime, int retryCount);
    }
}
