﻿namespace PrintTrackIdFromLog
{
    using System;
    using System.Configuration;
    using System.IO;
    using System.Linq;
    using System.Threading;

    public class DirSweeper : IDirSweeper
    {
        #region Members
        private readonly ILogger logger;
        private readonly IFileProcessorFactory fpFactory;
        private readonly string dir;
        private bool run = true;
        #endregion

        #region Constructor
        public DirSweeper(ILogger logger, IFileProcessorFactory fpFactory, string dir)
        {
            this.logger = logger ?? throw new ArgumentNullException("ILogger");
            this.fpFactory = fpFactory ?? throw new ArgumentNullException("IFileProcessorFactory");
          
            if (string.IsNullOrEmpty(dir))
                throw new ArgumentNullException("SweepDirectory");

            this.dir = dir;
        }
        #endregion

        #region IDirSweeper
        public void SweepFiles()
        {
            try
            {
                // var watchDir = ConfigurationManager.AppSettings.Get("watchDir");
                // var ext = ConfigurationManager.AppSettings.Get("fileExtension");
                var sleepTime = Convert.ToInt32(ConfigurationManager.AppSettings.Get("retryInterval"));
                var retryCount = Convert.ToInt32(ConfigurationManager.AppSettings.Get("retryCount"));
                var sweeperInterval = Convert.ToInt32(ConfigurationManager.AppSettings.Get("sweeperInterval"));
                var maxFiles = Convert.ToInt32(ConfigurationManager.AppSettings.Get("maxSweeperFiles"));

                while (run)
                {
                    var dirInfo = new DirectoryInfo(dir);

                    // process files
                    // var files = dirInfo.GetFiles(, SearchOption.TopDirectoryOnly).Where(f => !f.Name.Contains("SKIP")).OrderBy(f => f.CreationTime).Take(maxFiles);
                    var files = dirInfo.EnumerateFiles("*.*", SearchOption.TopDirectoryOnly)
                            .Where(f =>
                                (f.Name.Contains("Camera_LINE09"))
                            )
                            .OrderBy(f => f.CreationTime).Take(maxFiles);

                    foreach (var item in files)
                    {
                        try
                        {
                            File.Delete(item.FullName);

                            var p = fpFactory.Create(logger, item.FullName, item.Name, sleepTime, retryCount);

                            var t = new Thread(() =>
                            {
                                try
                                {
                                    p.ProcessFile();
                                }
                                finally
                                {
                                    p = null;
                                }
                            });
                            t.Start();

                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex);
                        }
                    }

                    Thread.Sleep(sweeperInterval);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }

        public void StopSweeper()
        {
            run = false;
        }
        #endregion
    }
}
