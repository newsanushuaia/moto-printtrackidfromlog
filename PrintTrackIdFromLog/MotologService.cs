﻿namespace PrintTrackIdFromLog
{
    using System.Configuration;
    using System.ServiceProcess;
    using System.Threading;
    using Castle.Windsor;
    using Castle.Windsor.Installer;
    public partial class MotologService : ServiceBase
    {
        public MotologService()
        {
            InitializeComponent();
        }


        public IDirSweeper FileSweeper { get; set; }

        protected override void OnStart(string[] args)
        {
            using (var container = GetContainer())
            {
                var dir = ConfigurationManager.AppSettings.Get("watchDirFile");
                var logger = container.Resolve<ILogger>();
                var fpFactory = container.Resolve<IFileProcessorFactory>();
                FileSweeper = container.Resolve<IDirSweeper>(new {logger,fpFactory,dir});
                var t = new Thread(FileSweeper.SweepFiles);
                t.Start();
            }
        }

        protected override void OnStop()
        {
            FileSweeper.StopSweeper();
        }

        public void Start()
        {
            using (var container = GetContainer())
            {
                var dir = ConfigurationManager.AppSettings.Get("watchDirFile");
                var logger = container.Resolve<ILogger>();
                var fpFactory = container.Resolve<IFileProcessorFactory>();
                FileSweeper = container.Resolve<IDirSweeper>(new { logger, fpFactory, dir });
                var t = new Thread(FileSweeper.SweepFiles);
                t.Start();
            }
        }


        private IWindsorContainer GetContainer()
        {
            return new WindsorContainer().Install(FromAssembly.This());
        }

    }
}
