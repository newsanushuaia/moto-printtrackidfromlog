﻿namespace PrintTrackIdFromLog
{
    using System;
    using System.Diagnostics;

    public class EventViewerLogger : ILogger
    {
        private static string appName = "PrintTrackIdFromLog";

        public void Debug(string text)
        {
            EventLog.WriteEntry(appName, text, EventLogEntryType.Information);
        }

        public void Warn(string text)
        {
            EventLog.WriteEntry(appName, text, EventLogEntryType.Warning);
        }

        public void Error(string text)
        {
            EventLog.WriteEntry(appName, text, EventLogEntryType.Error);
        }

        public void Error(Exception ex)
        {
            Error(string.Format("{0}\r\r{1}", ex.Message, ex.StackTrace));
        }
    }
}
