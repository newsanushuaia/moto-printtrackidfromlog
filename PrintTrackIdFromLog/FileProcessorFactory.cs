﻿namespace PrintTrackIdFromLog
{
    public class FileProcessorFactory : IFileProcessorFactory
    {
        public IFileProcessor Create(ILogger logger, string filePath, string fileName, int sleepTime, int retryCount)
        {
            return new FileProcessor(logger, filePath, fileName, sleepTime, retryCount);
        }
    }
}
